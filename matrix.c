#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int* matrix;
    int size;
    int rowLength;
} Matrix;

// TODO: pad smaller values within the matrix so all values are in line of one another
void displayMatrix(Matrix* matrix) {
    for (int i = 0; i < matrix->size / matrix->rowLength; i++) {
        for (int j = 0; j < matrix->rowLength; j++) {
            printf("%d ", *(matrix->matrix + (j + i * matrix->rowLength)));
        }
        puts(" ");
    }
    puts(" ");
}

// DEBUG: displays matrix array
void displayArr(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d, ", arr[i]);
    }
    puts(" ");
}

/*
 * Return codes:
 * 0 --> successful
 * 1 --> Matrices not equal
*/
int addMatrices(Matrix* result, Matrix* a, Matrix* b) {
    if ((a->size != b->size) || (a->rowLength != b->rowLength)) {
        return 1;
    }
    for (int i = 0; i < a->size; i++) {
        result->matrix[i] = a->matrix[i] + b->matrix[i];
    }
    return 0;
}

int subtractMatrices(Matrix* result, Matrix* a, Matrix* b) {
    if ((a->size != b->size) || (a->rowLength != b->rowLength)) {
        return 1;
    }
    for (int i = 0; i < a->size; i++) {
        result->matrix[i] = a->matrix[i] - b->matrix[i];
    }
    return 0;
}

/*
 * Calculates the determinant of a square matrix
 * Return codes:
 * 0 ---> successful
 * 1 ---> Not a square matrix, row != columns
*/
int determinant(int* det, Matrix* m) {
    if (m->size / m->rowLength != m->rowLength) {
        return 1;
    }
    if (m->rowLength == 2) {
        *det = m->matrix[0] * m->matrix[3] - m->matrix[1] * m->matrix[2];
    }
    return 0;
}

int main() {
    // defines a 2x2 matrix
    int arrA[4] = {1, 2, 3, 4};
    Matrix a;
    a.matrix = arrA;
    a.size = 4;
    a.rowLength = 2;

    // creates an integer
    int det = 0;
    int r = determinant(&det, &a);
    if (r == 0) {
        printf("|A| = %d\n", det);
    }
    return 0;
}
