# Matrix.c
A simple proof of concept program which allows for the declaration and manipulation of mathematical matrices

# Future?
In the future, if successful, this repo will be converted into a library, so that the code can be reused by others, if they desire.